package ar.com.grupoesfera.registrarme.adaptadores;

/**
 * Created by diego on 9/25/18.
 */
public interface AdaptadorParaRegistrarme {

    String MAIL_VALIDO = "pepe@pepe.com";
    String CLAVE_VALIDA = "p4SSw0RD";

    void noExisteUsuario(String usuario);

    void agregarUsuario(String usuario);

    void registrarme(String usuario);

    void registrarmeConContraseña(String clave);

    void usuarioSeCrea();

    void usuarioNoEstaRegistrado();

    void muestraMensaje(String mensaje);

    void meEncuentroEn(String vista);

    void borrarRepositorio();
}
